#!/bin/ksh
#set -x
[ -n "$TERM" ] && clear
########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/DenyLabels/SPF2.git
#
#	This file is part of "DenyLabel :: SPF2 Project"
#
# Date: 2018/08/30
#
##
###
########################################################################
###
##
#   Based on an idea from Stéphane Guedon @22decembre
##
###
########################################################################
###
##
#   For OpenBSD: ksh - public domain Korn Shell
##
###
########################################################################

########################################################################
###
##
#   Unchanged Variables; DO NOT TOUCH|MODIFY|CHANGE!!!
##
###
########################################################################

ROOT="$(dirname "$(readlink -f -- "$0")")"

DIR_LISTS="${ROOT}/lists"
DIR_SRC="${ROOT}/src"

now="$(date +"%x %X")"
today="$(date +%s)"
file_log="${ROOT}/spf_${today}.log"

### declare color variables; DO NOT TOUCH!
bold="$(tput bold)"
dim="$(tput dim)"
#green="$(tput setaf 2)"
neutral="$(tput sgr0)"
#red="$(tput setaf 1)"

typeset list="spf"
typeset q=""	# futur query

set -A ip4s ip6s

########## Include Configurables variables file ########################
. "${DIR_SRC}/config_vars"

########################################################################
###
##
#   Functions
##
###
########################################################################

append_array() {
	
	set -A array -- $1
	
	for ip in "${array[@]}"; do 
		if valid_ipv4 "$ip"; then 
			[[ "${ip4s[@]}" == *"$ip"* ]] || ip4s[${#ip4s[@]}+1]="$ip"; 
		fi
		if valid_ipv6 "$ip"; then 
			[[ "${ip6s[@]}" == *"$ip"* ]] || ip6s[${#ip6s[@]}+1]="$ip"; 
		fi
	done
	
	unset array ip
	
}

### Get data into file in array...
build_lists() {

    [ "${verbose}" -eq 1 ] && display_mssg "#" "*** Build list with src file: ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do
        
            if [ "${verbose}" -eq 1 ]; then
				if echo "${line}" | grep -v "^#"; then lists[$i]="${line}"; fi
			
			else
				if echo "${line}" | grep -qv "^#"; then lists[$i]="${line}"; fi
				
			fi
            let i++
        
        done < "${DIR_SRC}/${list}"
        
        unset i

    else

        display_mssg "KO" "The file ${DIR_SRC}/${list} is not readable!"
        
        byebye

    fi

}

byebye() {

	display_mssg "KO" " /!\ The script stop here  /!\ "
	display_mssg "KO" "Please, search the reasons..."
    
    exit 1;

}

display_mssg() {

    typeset statut info 
    statut="$1" info="$2" 
    
    case "${statut}" in
        "KO"|1)	color="${red}" 		;;
        "OK"|0)	color="${green}" 	;;
    esac

	if [ "${statut}" == "#" ]; then
		if [ "${OSN}" = "FreeBSD" ]; then
			printf "%s" "[ "; tput bold; printf "%s" "${info}"; tput sgr0; printf "%s" "] \n";
			
		else
			printf "[ ${bold}%s${neutral} ] \n" "${info}"
			
		fi
	
	elif [ "${statut}" == "hb" ]; then
		if [ "${OSN}" = "FreeBSD" ]; then
			tput dim; printf "%s \n" "${info}"; tput sgr0; printf "\n"
			
		else
			printf "${dim}%s${neutral} \n" "${info}"
			
		fi
		
	else
		if [ "${OSN}" = "FreeBSD" ]; then
			case "${statut}" in
				"KO")
					printf "%s" "[ "; tput setaf 1; printf "%s" "KO"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
				"OK")
					printf "%s" "[ "; tput setaf 2; printf "%s" "OK"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
			esac
			
		else
			printf "[ ${color}%s${neutral} ] %s \n" "${statut}" "${info}"
			
		fi
	
	fi

    unset info statut text

}

flush() {
	
	[ "$debug" -eq 1 ] && echo "### Date: $now ###" > "${file_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "#" "### Attempt to flush tables PF..."
	
	if [ $use_pf = 1 ]; then
		[ -z "${pf_table_ip4}" ] && pf_table_ip4="spf_ip4";
		[ -z "${pf_table_ip6}" ] && pf_table_ip6="spf_ip6";
		
		pfctl -t $pf_table_ip4 -T flush 
		pfctl -t $pf_table_ip6 -T flush 
	
	fi
	
	[ "${verbose}" -eq 1 ] && display_mssg "#" "### Attempt to delete lists..."
	rm -f ${DIR_LISTS}/${list}_ip{4,6}
	
}

mng_lists() {
	
	count="${#lists[@]}"

    if [ "${count}" -gt 0 ]; then
    
		for domain in "${lists[@]}"; do
			query txt $domain
			threat
		done
    
    fi
	
}

pf_action() {
	
	if [ $use_pf = 0 ]; then
		pfctl -t "${list}_${sub}" -T replace -f "$ROOT/${list}_${sub}"
				
	else
		pfctl -t "${list}_${sub}" -T add 
				
	fi
	
}

query() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "hb" "### Using dig for $2..."
	
	typeset d="$2" o="-t $1"
	[ "${debug}" -eq 1 ] && echo "domain: $d; option: $o" >> "${file_log}"
	
	q="$(dig $d +short $o | tr -d '"')" 
	
	unset d o

}

threat() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "hb" "#### Threat query dig... to parse informations SPF"
	
	set -A spf -- $(echo "$q")
	
	for str in "${spf[@]}"; do
	
		case "$str" in 
		
			*"a:"*|*"ptr"*)
			
				case  "$str" in
					*"/"*)
						if [[ "$str" == *"/"* ]]; then
							prefix="$(echo "$str" | awk -F '/' '{ print $2 }')"
							
							l1="$(echo "$str" | wc -c)"
							l2="$(echo "$prefix" | wc -c)"
							length="$(echo "$l1 - $l2 - 2" | bc)"
							unset l1 l2
						
							dom="$(echo "$str" | awk -F ':' '{ print substr($2, 0, '"$length"') }')"
							unset length
						fi
					;;
					"ptr")
						dom="$(echo $domain)"
					;;
					*)
						dom="$(echo "$str" | awk -F ':' '{ print $2 }')"
					;;
				esac
				
				if [ -n "$prefix" ]; then
					[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom | prefix: $prefix" >> "${file_log}"
					
					info="$(dig $dom +short -t a)/$prefix"
				
					[ "${debug}" -eq 1 ] && echo "$(echo "$str" | awk '{ print substr($0, 0, 2) }') :: ip4: $info" >> "${file_log}"
				
					if [ -n "$info" ]; then append_array $info; fi
				
					info="$(dig $dom +short -t aaaa)$/prefix"
				
					[ "${debug}" -eq 1 ] && echo "$(echo "$str" | awk '{ print substr($0, 0, 2) }') :: ip6: $info" >> "${file_log}"
				
					if [ -n "$info" ]; then  append_array $info; fi
					
					unset prefix
				
				else
					[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom" >> "${file_log}"
				
					info="$(dig $dom +short -t a)"
				
					[ "${debug}" -eq 1 ] && echo "$(echo "$str" | awk '{ print substr($0, 0, 2) }') :: ip4: $info" >> "${file_log}"
				
					if [ -n "$info" ]; then append_array $info; fi
				
					info="$(dig $dom +short -t aaaa)"
				
					[ "${debug}" -eq 1 ] && echo "$(echo "$str" | awk '{ print substr($0, 0, 2) }') :: ip6: $info" >> "${file_log}"
				
					if [ -n "$info" ]; then  append_array $info; fi
				
				fi
				
				unset dom ips
			
			;;
		
			*"include"*)
				
				dom="$(echo "$str" | awk -F ':' '{ print $2 }')"; 
				[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom" >> "${file_log}"
				
				query txt $dom
				[ "${debug}" -eq 1 ] && echo "query: $q" >> "${file_log}"
				threat
				
				unset dom
				
			;;
			
			*"ip4"*|*"ip6"*)
				
				sub="$(echo "$str" | awk '{ print substr($1, 0, 3) }')";
				
				case "$sub" in
					"ip4")
						ip="$(echo "$str" | awk '{ print substr($0, 5) }')"
						[ "${debug}" -eq 1 ] && echo "ip(4): $ip" >> "${file_log}"
						
						# if ip not in array... append to array
						[[ "${ip4s[@]}" == *"$ip"* ]] || ip4s[${#ip4s[@]}+1]="$ip"
						unset ip
					;;
					"ip6")
						ip="$(echo "$str" | awk '{ print substr($0, 5) }')"
						[ "${debug}" -eq 1 ] && echo "ip(6): $ip" >> "${file_log}"
						
						[[ "${ip6s[@]}" == *"$ip"* ]] || ip6s[${#ip6s[@]}+1]="$ip"
						unset ip
					;;
				esac
				
				unset sub
			;;
			
			*"mx"*)

				case "$str" in
					"mx") 
						dom="$(echo $domain)"

					;;
					*"mx/"*)
						dom="$(echo $domain)"
						prefix="$(echo "$str" | awk -F '/' '{ print $2 }')"

					;;
					*)
						if [[ "$str" == *"/"* ]]; then
							prefix="$(echo "$str" | awk -F '/' '{ print $2 }')"
							
							l1="$(echo "$str" | wc -c)"
							l2="$(echo "$prefix" | wc -c)"
							length="$(echo "$l1 - $l2 - 3" | bc)"
							unset l1 l2
							
							dom="$(echo "$str" | awk -F ':' '{ print substr($2, 0, '"$length"') }')"
							unset length
													
						else
							dom="$(echo "$str" | awk -F ':' '{ print $2 }')"
							
						fi
					;;
				esac
				
				
				if [ -n "$prefix" ]; then
					[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom | prefix: $prefix" >> "${file_log}"
				
					info="$(dig $(dig $dom +short -t mx) +short -t a)/$prefix"
					
					unset prefix
				
				else
					[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom" >> "${file_log}"
							
					info="$(dig $(dig $dom +short -t mx) +short -t a)"
				
				fi
				
				[ "${debug}" -eq 1 ] && echo "mx :: ips: $info" >> "${file_log}"
				
				if [ -n "$info" ]; then append_array $info; fi
				
				unset dom info 
				
			;;
		
			*"redirect"*)
			
				dom="$(echo "$str" | awk -F '=' '{ print $2 }')"; 
				[ "${debug}" -eq 1 ] && echo "domain: $domain | str: $str | dom: $dom" >> "${file_log}"
				
				query txt $dom
				[ "${debug}" -eq 1 ] && echo "query: $q" >> "${file_log}"
				threat
				
				unset dom
				
			;;
		
		esac
		
		unset str
	
	done
	
	unset spf
	
}

valid_ipv4() {
	
	echo "$1" | egrep '^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/?([0-9]{1,2})?)$' #"${1}"
	
}
	
valid_ipv6() {

	echo "$1" | egrep '^([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])$' #"${1}"
	
}

write_datas() {
	
	[ "${verbose}" -eq 1 ] && display_mssg "#" "### Writing datas... into the list file and/or PF!"
	
	# manage IPv4 datas
	count="${#ip4s[@]}"
	
	if [ "${count}" -ge 1 ]; then 
	
		# sort array 
		set -A array -- $(printf "%s\n" "${ip4s[@]}" | sort -n)
	
		for ip in "${array[@]}"; do
			
			if valid_ipv4 "$ip"; then
				if [ $use_pf -eq 1 ]; then pfctl -t $pf_table_ip4 -T add $ip; fi
					
				echo "$ip" >> "${DIR_LISTS}/${list}_ip4";

			fi
			
		done	
		
		unset array
		
	fi
	
	# manage IPv6 datas
	count="${#ip6s[@]}"
	
	if [ "${count}" -ge 1 ]; then 
	
		# sort array
		set -A array -- $(printf "%s\n" "${ip6s[@]}" | sort -n)
	
		for ip in "${array[@]}"; do
			
			if valid_ipv6 "$ip"; then
				if [ $use_pf -eq 1 ]; then pfctl -t $pf_table_ip6 -T add $ip; fi
					
				echo "$ip" >> "${DIR_LISTS}/${list}_ip6";

			fi
			
		done
		
		unset array
		
	fi
	
}

########################################################################
###
##
#						 ^             ^
#   					/!\ Execution /!\
#   					---           ---
##
###
########################################################################

flush
build_lists
mng_lists
write_datas
