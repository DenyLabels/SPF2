BlackLabel :: SPF2 
==================

(Sender Policy Framework for Firewalling)

---------------
--- English ---
---------------

=> Purpose: generate lists of adresses IP (v4, v6) by SPF Lookups

Both lists are created into directory 'lists/':
  - 'spf_ip4' for IPv4
  - 'spf_ip6' for IPv6

=> Config variables:

Some variables are configurables; please, see file 'src/config_vars'.
  - 'use_pf': inject directly into tables PF; 1: enable; 0: disable
  - 'pf_table_ip4': table name, managed by PF, for IPv4 traffic
  - 'pf_table_ip6': table name, managed by PF, for IPv6 traffic

----------------
--- Français ---
----------------

Script 'spf'
------------

=> Ce script a pour but de générer une liste d'adresses IP (v4, v6) après avoir récupèrer les informations SPF.

Retrouvez celles-ci dans le répertoire 'lists/' :
  - 'spf_ip4' pour les adresses IPv4
  - 'spf_ip6' pour les adresses IPv6

=> Configurables variables:

Quelques variables sont configurables ; voir le fichier 'src/config_vars'. 
  - 'use_pf' : injecter les adresses IP dans les tables du pare-feu - 1 pour activer ; 0 pour désactiver
  - 'pf_table_ip4' : nom de la table gérée par le pare-feu, pour le trafic IPv4
  - 'pf_table_ip6' : nom de la table gérée par le pare-feu, pour le trafic IPv6

----------------------
--- Règles | Rules ---
----------------------

=> gestion par fichier / by file:

	table <spf_4> persist file "/dir_to_spf2/lists/spf_ip4"
	table <spf_6> persist file "/dir_to_spf2/lists/spf_ip6"

=> gestion par table / by table:

    table <spf_4> persist
    table <spf_6> persist

=> Règles PF communes / Commons rules PF:

	pass in log inet  proto tcp from  <spf_4> to egress port smtp
    pass in log inet6 proto tcp from  <spf_6> to egress port smtp
